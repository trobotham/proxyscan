package main

import (
	"fmt"
)

func http_put_scan(settings scan_settings, port int, name string, use_tls bool) {
	buf := fmt.Sprintf("PUT http://%s:%d/ HTTP/1.0\r\n" +
						"Content-length: 0\r\n" +
						"Connection: close\r\n" +
						"\r\n",
						settings.destIp, settings.destPort)
	scan(settings, port, use_tls, name, []byte(buf))
}

func http_put(settings scan_settings, port int) {
	http_put_scan(settings, port, "http_put", false)
}

