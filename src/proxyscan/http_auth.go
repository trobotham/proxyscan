package main

import (
	"net"
	"strings"
)

func send_request(con net.Conn, buf string) string {
	con.Write([]byte(buf))

	reply := make([]byte, 1024)
	_, err := con.Read(reply)
	if err != nil {
		return ""
	}

	return string(reply)
}

func http_auth(settings scan_settings, port int) {
	con := connect(settings, port, "http_auth", false)
	if con == nil {
		return
	}
	defer con.Close()

	reply := send_request(con, "GET / HTTP/1.0\r\n\r\n")

	if !strings.HasPrefix(reply, "HTTP/1.0 401") {
		return
	}

	con = connect(settings, port, "http_auth", false)
	if con == nil {
		return
	}
	defer con.Close()

	reply = send_request(con, "GET / HTTP/1.0\r\n" +
					"Authorization: Basic YWRtaW46cGFzc3dvcmQ=\r\n" +
					"\r\n")
	if !strings.HasPrefix(reply, "HTTP/1.0 200") {
		return
	}

	hit(settings, "http_auth", port)
}
