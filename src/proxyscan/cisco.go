package main

import (
	"fmt"
)

func cisco(settings scan_settings, port int) {
	buf := fmt.Sprintf("cisco\r\n" +
						"telnet %s %d\r\n",
						settings.destIp, settings.destPort)
	scan(settings, port, false, "cisco", []byte(buf))
}
